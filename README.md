# Simple project in C++ and OpenSim

## Compiling in Visual Studio Code
1. Install the extension CMake Tools here: https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools
2. When saving the CMakeLists.txt file, a build folder should be created or updated automatically.
3. On the bottom bar of Visual Studio Code (in blue), there should be a little arrow. Click on it (you can select which executable to launch on its right), and the executable will be created/updated then executed.

## Compiling manually (terminal)
1. Go inside the directory with the terminal;
2. Initialize the building with CMake: 
```
cmake -S ./ -B ./build
```
> What does it do? CMake initializes from the source file located in the root folder (``-S ./``) and put the configuration files inside a build folder (``-B ./build``). The executable will be put inside the build folder.
3. Go into the build folder: ``cd build/``
4. Create the executable: ``make``
5. Launch the executable (called 'main' here, but you can define it otherwise in CMakeLists.txt): ``./main``